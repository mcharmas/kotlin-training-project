package pl.charmas.training.kotlintraining.basicsyntax

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

fun hello(): String = TODO()

/**
 * Zadanie 1.
 * Zaimplementuj funkcję hello aby zwracała napis "Hello world!"
 * Jeśli to konieczne dopisz testy jednostkowe.
 *
 * Zadanie 2.
 * Funkcja hello powinna przyjmować parametr name i witać się w formacie "Hello name!".
 * Użyj interpolacji napisów.
 * Jeśli to konieczne dopisz testy jednostkowe.
 *
 * Zadanie 3.
 * Funkcja hello powinna przyjmować null jako parametr name. Wtedy zwracać napis "Hello world!"
 * Jeśli to konieczne dopisz testy jednostkowe.
 *
 * Zadanie 4.
 * Napisz funkcję zwracającą minimum z dwóch intów.
 * Dopisz testy jednostkowe.
 */
class Part1 {
    @Test
    fun helloFromKotlin() {
        assertEquals("Hello world!", hello())
    }
}
