package pl.charmas.training.kotlintraining.basicsyntax

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class Client(val personalInfo: PersonalInfo?)
class PersonalInfo(val email: String?)
interface Mailer {
    fun sendMessage(email: String, message: String)
}

fun sendMessageToClient(client: Client?, message: String?, mailer: Mailer): Unit = TODO()

// public void sendMessageToClient(
// @Nullable Client client,
// @Nullable String message,
// @NotNull Mailer mailer
// ) {
//     if (client == null || message == null) return;
//
//     PersonalInfo personalInfo = client.getPersonalInfo();
//     if (personalInfo == null) return;
//
//     String email = personalInfo.getEmail();
//     if (email == null) return;
//
//     mailer.sendMessage(email, message);
// }

/**
 * Zadanie 1.
 * Zaimplementuj powyższą funkcję javową w koltinie używając tylko jednego if'a
 * kożystając z bezpiecznego dostępu do referencji.
 *
 * Zadanie 2.
 * Pozbądź się nawet ostatniego if'a.
 */
class Part5 {

    @Test
    fun sendsEmailWhenEmailAndMessageGiven() {
        //given
        val email = "sample@email.com"
        val message = "message"
        val mailer = MockMailer()

        //when
        sendMessageToClient(Client(PersonalInfo(email)), message, mailer)

        //then
        assertTrue(mailer.called)
        assertEquals(email, mailer.calledWithEmail)
        assertEquals(message, mailer.calledWithMessage)
    }

    @Test
    fun doesntSendEmailWhenNoMessageOrClientGiven() {
        //given
        val mailer = MockMailer()

        //when
        sendMessageToClient(null, null, mailer)

        //then
        assertFalse(mailer.called)
    }

    class MockMailer : Mailer {
        var called: Boolean = false
        var calledWithEmail: String? = null
        var calledWithMessage: String? = null

        override fun sendMessage(email: String, message: String) {
            this.called = true
            this.calledWithEmail = email
            this.calledWithMessage = message
        }
    }
}
