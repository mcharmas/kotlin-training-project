package pl.charmas.training.kotlintraining.basicsyntax

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

fun toJson(content: Any?): String = TODO()

/**
 * Zadanie 1
 * Przeiteruj po mapie i zamień ją na string zawierający JSONA
 *
 * Zadanie 2
 * Rozszerz serializer tak aby wspierał:
 *  * Map
 *  * List
 *  * Integer
 *  * String (bez escapowania)
 *  * null
 *
 *  Zadanie 3
 *  Dodaj extension function do obiektu Map i List do łatwiejszej serializacji
 *  obiektów.
 */
class Part8 {

    @Test
    fun serializesMap() {
        val serializationResult = toJson(mapOf("test" to "test", "test1" to "test1"))
        val expectedResult = """{"test":"test", "test1":"test1"}"""
        assertEquals(expectedResult, serializationResult)
    }

    @Test
    fun serializesList() {
        val serializationResult = toJson(listOf(1, 2, 3, 4))
        val expectedResult = """[1, 2, 3, 4]"""
        assertEquals(expectedResult, serializationResult)
    }

    @Test
    fun serializesNull() {
        val serializationResult = toJson(mapOf("test" to null))
        val expectedResult = """{"test":null}"""
        assertEquals(expectedResult, serializationResult)
    }

    @Test
    fun withExtensionMethod() {
        TODO()
//        val listSerializationResult = listOf("a", "b").toJson()
//        val mapSerializationResult = mapOf("a" to "b").toJson()
//        assertTrue(listSerializationResult.isNotEmpty())
//        assertTrue(mapSerializationResult.isNotEmpty())
    }
}
