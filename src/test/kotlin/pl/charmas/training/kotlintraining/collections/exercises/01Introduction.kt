package pl.charmas.training.kotlintraining.collections.exercises

import java.util.HashSet

/*
 *  There are many operations that help to transform one collection into another, starting with 'to'
 */
fun example0(list: List<Int>) {
    list.toSet()

    list.toCollection(HashSet<Int>())
}

fun Shop.getSetOfCustomers(): Set<Customer> {
    // Return a set containing all the customers of this shop
    todoCollectionTask()
//    return this.customers
}

