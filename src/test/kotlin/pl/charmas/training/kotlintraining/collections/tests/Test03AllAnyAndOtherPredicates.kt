package pl.charmas.training.kotlintraining.collections.tests

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import pl.charmas.training.kotlintraining.collections.exercises.City
import pl.charmas.training.kotlintraining.collections.exercises.checkAllCustomersAreFrom
import pl.charmas.training.kotlintraining.collections.exercises.countCustomersFrom
import pl.charmas.training.kotlintraining.collections.exercises.findFirstCustomerFrom
import pl.charmas.training.kotlintraining.collections.exercises.hasCustomerFrom
import pl.charmas.training.kotlintraining.collections.exercises.isFrom

class Test03AllAnyAndOtherPredicates {
    @Test
    fun testCustomerIsFromCity() {
        assertTrue(customers[lucas]!!.isFrom(Canberra))
        assertFalse(customers[lucas]!!.isFrom(Budapest))
    }

    @Test
    fun testAllCustomersAreFromCity() {
        assertFalse(shop.checkAllCustomersAreFrom(Canberra))
    }

    @Test
    fun testAnyCustomerIsFromCity() {
        assertTrue(shop.hasCustomerFrom(Canberra))
    }

    @Test
    fun testCountCustomersFromCity() {
        assertEquals(2, shop.countCustomersFrom(Canberra))
    }

    @Test
    fun testFirstCustomerFromCity() {
        assertEquals(customers[lucas], shop.findFirstCustomerFrom(Canberra))
        assertEquals(null, shop.findFirstCustomerFrom(City("Chicago")))
    }
}
