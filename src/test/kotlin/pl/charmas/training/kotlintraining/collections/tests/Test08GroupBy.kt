package pl.charmas.training.kotlintraining.collections.tests

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import pl.charmas.training.kotlintraining.collections.exercises.groupCustomersByCity

class Test08GroupBy {
    @Test
    fun testGroupCustomersByCity() {
        assertEquals(groupedByCities, shop.groupCustomersByCity())
    }
}
