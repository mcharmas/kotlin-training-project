package pl.charmas.training.kotlintraining.collections.tests

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import pl.charmas.training.kotlintraining.collections.exercises.getCustomersWithMoreUndeliveredOrdersThanDelivered

class Test09Partition {
    @Test
    fun testGetCustomersWhoHaveMoreUndeliveredOrdersThanDelivered() {
        assertEquals(setOf(customers[reka]), shop.getCustomersWithMoreUndeliveredOrdersThanDelivered())
    }
}
