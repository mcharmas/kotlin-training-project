package pl.charmas.training.kotlintraining.collections.tests

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import pl.charmas.training.kotlintraining.collections.exercises.getSetOfProductsOrderedByEachCustomer


class Test10Fold {
    @Test
    fun testGetProductsOrderedByAllCustomers() {
        val testShop = shop(
            "test shop for 'fold'",
            customer(
                lucas, Canberra,
                order(idea),
                order(webStorm)
            ),
            customer(
                reka, Budapest,
                order(idea),
                order(youTrack)
            )
        )
        assertEquals(setOf(idea), testShop.getSetOfProductsOrderedByEachCustomer())
    }
}
