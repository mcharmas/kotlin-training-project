package pl.charmas.training.kotlintraining.collections.tests

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import pl.charmas.training.kotlintraining.collections.exercises.getCustomersWhoOrderedProduct
import pl.charmas.training.kotlintraining.collections.exercises.getMostExpensiveDeliveredProduct
import pl.charmas.training.kotlintraining.collections.exercises.getNumberOfTimesProductWasOrdered

class Test11CompoundTasks {
    @Test
    fun testGetCustomersWhoOrderedProduct() {
        assertEquals(setOf(customers[reka], customers[asuka]), shop.getCustomersWhoOrderedProduct(idea))
    }

    @Test
    fun testMostExpensiveDeliveredProduct() {
        val testShop = shop(
            "test shop for 'most expensive delivered product'",
            customer(
                lucas, Canberra,
                order(idea, isDelivered = false),
                order(reSharper)
            )
        )
        assertEquals(reSharper, testShop.customers[0].getMostExpensiveDeliveredProduct())
    }

    @Test
    fun testNumberOfTimesEachProductWasOrdered() {
        assertEquals(4, shop.getNumberOfTimesProductWasOrdered(idea))
    }

    @Test
    fun testNumberOfTimesEachProductWasOrderedForRepeatedProduct() {
        assertEquals(3, shop.getNumberOfTimesProductWasOrdered(reSharper))
    }

    @Test
    fun testNumberOfTimesEachProductWasOrderedForRepeatedInOrderProduct() {
        assertEquals(3, shop.getNumberOfTimesProductWasOrdered(phpStorm))
    }
}
