package pl.charmas.training.kotlintraining.fp

import org.junit.jupiter.api.Test

open class View {
    var onClickListener: OnClickListener? = null

    fun click() = onClickListener?.onClicked(this)

    interface OnClickListener {
        fun onClicked(v: View)
    }
}

/**
 * Zadanie 1.
 * Zmień implementację widoku powyżej tak, aby OnClickListener był funkcją
 * a nie interfacem.
 *
 * Zadanie 2.
 * Nadaj typealias zadeklarowanemy typowi.
 */
class Part16 {
    @Test
    fun viewIsClickable() {
        val view = View()
        view.onClickListener = object : View.OnClickListener {
            override fun onClicked(v: View) = println("Clicked!")
        }
        view.click()
    }
}
