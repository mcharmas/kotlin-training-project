package pl.charmas.training.kotlintraining.generics

import com.google.gson.Gson
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

data class Product(val name: String, val price: BigDecimal)

/**
 * Zadanie 1.
 * Dodaj Extension function z reifikowanym typem generycznym do Stringa
 * umożliwiającą zamianę go na obiekt (deserializację).
 *
 * Składnia wywołania:
 * val deserialized: Product = serialized.fromJson()
 *
 * Typ generyczny powinien zostać zgadnięty przez kompilator.
 */
class Part21 {

    val gson = Gson()

    @Test
    fun serializesAndDeserializes() {
        val source = Product("asdf", BigDecimal(21))
        val serialized = gson.toJson(source)
        val deserialized = gson.fromJson(serialized, Product::class.java)
        assertEquals(source, deserialized)
    }
}
