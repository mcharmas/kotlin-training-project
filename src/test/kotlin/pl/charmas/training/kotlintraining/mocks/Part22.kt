package pl.charmas.training.kotlintraining.mocks

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.Date

class Sample {
    fun example(): Int = 1
}

interface TimeProvider {
    fun getCurrentTime(): Date
}

data class Order(val creationTime: Date)

class OrderFactory(private val timeProvider: TimeProvider) {
    fun createNewOrder() = Order(timeProvider.getCurrentTime())
}

/**
 * Zadanie 1.
 * Przetestuj w izolacji fabrykę zamówień.
 */
class Part22 {

    @Test
    fun mocksFinalClass() {
        val mockedSample = mockk<Sample> {
            every { example() } returns 2
        }

        assertEquals(2, mockedSample.example())
    }

    @Test
    fun createdOrderShouldHaveCurrentDate() {
        TODO()
    }
}
