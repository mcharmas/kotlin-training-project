package pl.charmas.training.kotlintraining.oop

/** Object Oriented programming */
// 1. Basic class syntax (Invoice)
// 2. Constructors
// 3. Initial block
// 4. Secondary constructors
// 5. Properties as parameters
// 6. Inner properties
// 7. Visibility modifiers
// 8. Creating instances - no new keyword
// 9. Equality (structural / referential)
// 10. Inheritance
// 12. Methods overriding
// 13. Properties overriding
// 14. Abstract classes
// 15. Data class
// 16. Interfaces
// 17. Objects
// 18. Sealed Class
// 19. Nested classes
// 20. Class Delegation
// 21. Property delegation

/** Design Patterns */
// Decorator Pattern
// Observer Pattern
