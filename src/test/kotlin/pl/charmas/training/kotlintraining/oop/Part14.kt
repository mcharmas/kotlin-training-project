package pl.charmas.training.kotlintraining.oop

/**
 * Zadanie 1
 * Użyj delegata właściwości do leniwego pobrania danych z intenta uruchamianego
 * HelloActivity i wyświetl przekazane dane w TextView.
 *
 * Zadanie 2
 * Przenieś tworzenie intentu z argumentami do metody w Companion Object w Activity docelowym.
 *
 * Zadanie 3
 * Wyciągnij napis z nazwą argumentu do stałej w Activity docelowym
 *
 * Zadanie 4
 * Zaimplementuj własnego delegata ReadWriteProperty, który będzie zapisywał dane w SharedPreferences
 */
class Part14 {

}
