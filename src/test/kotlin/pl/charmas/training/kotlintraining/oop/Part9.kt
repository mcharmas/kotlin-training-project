package pl.charmas.training.kotlintraining.oop

/**
 * Zadanie 1.
 * Zaimplementuj klasę prostokąt posiadającą wysokość i szerokość i utwórz jej instancję
 *
 * Zadanie 2.
 * Utwórz interface obiektu potrafiącego policzyć własne pole, niech prostokąt implementuje
 * ten interface.
 *
 * Zadanie 3.
 * Dodaj właściwość do prostokąta, która będzie mówić czy jest on kwadratem
 *
 * Zadanie 4.
 * Dodaj factory method do Companion Objectu prostokąta tworzącą kwadrat o podanym wymiarze.
 *
 * Zadanie 5.
 * Utwórz object wewnątrz interface'u dotyczącego pola, który będzie służył do porównywania
 * figur po ich polu powierzchni.
 */
class Part9 {
}
